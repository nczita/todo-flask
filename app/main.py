from flask import Flask
from flask import jsonify, abort
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
CORS(app)

todoItems = json.load(open('todo_items.json'))

@app.route("/todos")
@cross_origin()
def todos():
    return jsonify(todoItems)

@app.route("/todos/<id>")
@cross_origin()
def get_todo(id):
    searched_todo = list(filter(lambda t: t["id"] == id, todoItems))
    if searched_todo:
        return jsonify(searched_todo[0])
    else:
        abort(404)