PACTICIPANT := "todo-provider"
PACT_CLI="docker run --rm -v ${PWD}:${PWD} -e PACT_BROKER_BASE_URL -e PACT_BROKER_PUBLISH_VERIFICATION_RESULTS pactfoundation/pact-cli:latest"

all: pact_verify

pact_verify: .env
	docker-compose build --pull
	docker-compose up --abort-on-container-exit --exit-code-from pact_verifier
	docker-compose logs pact_verifier

deploy_app:
	@echo "Deploying to prod"


pact_can_i_deploy: .env
	@"${PACT_CLI}" broker can-i-deploy \
	  --pacticipant ${PACTICIPANT} \
	  --version ${GIT_COMMIT} \
	  --to-environment production \
	  --retry-while-unknown 0 \
	  --retry-interval 10

pact_tag: 
	@"${PACT_CLI}" broker create-version-tag \
	  --pacticipant ${PACTICIPANT} \
	  --version ${GIT_COMMIT} \
	  --auto-create-version \
	  --tag ${GIT_BRANCH}

pact_record_deployment:
	@"${PACT_CLI}" broker record-deployment \
    --pacticipant ${PACTICIPANT} --version ${GIT_COMMIT} \
    --environment production

